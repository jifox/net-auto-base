# Network Automation - Python-3 Container

User agnostic base container used for network automation

Update with: 

```bash
docker tag net-auto-base:tagname jifox01/net-auto-base:tagname
docker push jifox01/net-auto-base:tagname
```

## Installed Python Libs

```bash
docker run --rm -it jifox01:net-auto-base:0.1.5 pip freeze

ansible==2.10.1
ansible-base==2.10.2
appdirs==1.4.4
argcomplete==1.12.1
astroid==2.4.2
asyncssh==2.4.2
attrs==20.2.0
bcrypt==3.2.0
black==20.8b1
cached-property==1.5.2
certifi==2020.6.20
cffi==1.14.3
chardet==3.0.4
ciscoconfparse==1.5.19
click==7.1.2
colorama==0.4.4
commonmark==0.9.1
cryptography==3.2.1
distro==1.5.0
dnspython==2.0.0
docker==4.3.1
docker-compose==1.27.4
dockerpty==0.4.1
docopt==0.6.2
furl==2.1.0
future==0.18.2
idna==2.10
ISE==0.1.2
isort==5.6.4
Jinja2==2.11.2
jmespath==0.10.0
jsonschema==3.2.0
junos-eznc==2.5.4
lazy-object-proxy==1.4.3
lxml==4.6.1
MarkupSafe==1.1.1
mccabe==0.6.1
mypy-extensions==0.4.3
napalm==3.2.0
napalm-ansible==1.1.0
ncclient==0.6.9
netaddr==0.8.0
netmiko==3.3.2
nornir==3.0.0
nornir-ansible==2020.9.26
nornir-jinja2==0.1.0
nornir-napalm==0.1.1
nornir-netbox==0.1.1
nornir-netmiko==0.1.1
nornir-scrapli==2020.10.10
nornir-utils==0.1.1
ntc-templates==1.6.0
objectpath==0.6.1
orderedmultidict==1.0.1
packaging==20.4
paramiko==2.7.2
passlib==1.7.4
pathspec==0.8.0
pycparser==2.20
pyeapi==0.8.3
Pygments==2.7.2
pylint==2.6.0
PyNaCl==1.4.0
pynetbox==5.1.0
pyparsing==2.4.7
pyrsistent==0.17.3
pyserial==3.4
python-dotenv==0.15.0
PyYAML==5.3.1
regex==2020.10.28
requests==2.24.0
rich==9.1.0
rope==0.18.0
ruamel.yaml==0.16.12
scp==0.13.3
scrapli==2020.10.10
scrapli-asyncssh==2020.10.10
scrapli-community==2020.9.19
scrapli-netconf==2020.10.24
six==1.15.0
tenacity==6.2.0
textfsm==1.1.0
texttable==1.6.3
toml==0.10.1
transitions==0.8.4
typed-ast==1.4.1
typing-extensions==3.7.4.3
urllib3==1.25.11
websocket-client==0.57.0
wrapt==1.12.1
xmltodict==0.12.0
yamllint==1.25.0
yamlordereddictloader==0.4.0
yapf==0.30.0
yq==2.11.1
```
