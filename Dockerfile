FROM python:3

ENV DEBIAN_FRONTEND=noninteractive

# Install apt-utils to allow immediate package configuration
COPY *.txt /tmp/
COPY library-scripts/* /tmp/library-scripts/
RUN apt-get update \
    && apt-get install -yq apt-utils \
    && xargs -d '\n' -n 1 -- apt-get install -yq < /tmp/packages-apt.txt \
    # Clean up
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/* \
    # Set locales
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

RUN pip install --upgrade --no-cache pip \
    && pip install --no-cache -r /tmp/requirements.txt

ENV DEBIAN_FRONTEND=
